package com.example.apploverassignment.network

import retrofit2.Retrofit

interface ClientProvider {

    fun getClient(): Retrofit
}
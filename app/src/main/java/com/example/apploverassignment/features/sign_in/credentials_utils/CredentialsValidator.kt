package com.example.apploverassignment.features.sign_in.credentials_utils

class CredentialsValidator(private val listener: OnCredentialsNotValidListener) {

    fun areCredentialsValid(email: String, password: String): Boolean {
        if (!email.isValidEmail()) {
            listener.onCredentialsNotValid(CredentialsError.EMAIL)
            return false
        }
        if (password.isEmpty()) {
            listener.onCredentialsNotValid(CredentialsError.PASSWORD)
            return false
        }
        if (password.length < 5) {
            listener.onCredentialsNotValid(CredentialsError.PASSWORD_LENGTH)
            return false
        }
        return true
    }
}
package com.example.apploverassignment.features.sign_in.credentials_utils

enum class CredentialsError {
    EMAIL, PASSWORD, PASSWORD_LENGTH
}
package com.example.apploverassignment.features.sign_in

interface SignInContract {

    interface View {
        fun postErrorMessage(message: String)
        fun showInputGroup()
        fun hideInputGroup()
        fun getSharedView(): android.view.View?
    }

    interface Presenter {
        fun postCredentials(email: String, password: String)
    }
}
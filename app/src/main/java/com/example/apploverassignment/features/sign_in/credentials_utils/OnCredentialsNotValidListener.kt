package com.example.apploverassignment.features.sign_in.credentials_utils

interface OnCredentialsNotValidListener {

    fun onCredentialsNotValid(error: CredentialsError)
}
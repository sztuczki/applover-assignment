package com.example.apploverassignment.features.sign_in

import com.example.apploverassignment.R
import com.example.apploverassignment.data.model.Credentials
import com.example.apploverassignment.features.sign_in.credentials_utils.CredentialsError
import com.example.apploverassignment.features.sign_in.credentials_utils.CredentialsValidator
import com.example.apploverassignment.features.sign_in.credentials_utils.OnCredentialsNotValidListener
import com.example.apploverassignment.utils.navigation_utils.Navigator
import com.example.apploverassignment.network.ClientProvider
import com.example.apploverassignment.service.ApitomeService
import com.example.apploverassignment.utils.string_utils.StringResourceProvider
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SignInPresenter(
    private val clientProvider: ClientProvider,
    private val stringProvider: StringResourceProvider,
    private val navigator: Navigator,
    private val view: SignInContract.View
) : SignInContract.Presenter, OnCredentialsNotValidListener {

    private val validator: CredentialsValidator = CredentialsValidator(this)

    override fun postCredentials(email: String, password: String) {
        if (validator.areCredentialsValid(email, password)) {
            view.hideInputGroup()
            val credentials = Credentials(email, password)
            val retrofit = clientProvider.getClient()
            val service = retrofit.create(ApitomeService::class.java)
            service.signIn(credentials).enqueue(object : Callback<Credentials> {
                override fun onResponse(call: Call<Credentials>, response: Response<Credentials>) {
                    val responseCode = response.code()
                    handleResponse(responseCode)
                }

                override fun onFailure(call: Call<Credentials>, t: Throwable) {
                    //TODO: add check via network info?
                    //TODO: add other related messages depending on the actual error thrown
                    view.showInputGroup()
                    view.postErrorMessage(stringProvider.getStringResource(R.string.error_no_internet))
                }
            })
        }
    }

    override fun onCredentialsNotValid(error: CredentialsError) {
        val message: String = when (error) {
            CredentialsError.EMAIL -> {
                stringProvider.getStringResource(R.string.error_wrong_email)
            }
            CredentialsError.PASSWORD -> {
                stringProvider.getStringResource(R.string.error_no_password)
            }
            CredentialsError.PASSWORD_LENGTH -> {
                stringProvider.getStringResource(R.string.error_password_too_short)
            }
        }
        view.postErrorMessage(message)
    }

    private fun handleResponse(code: Int) {
        when (code) {
            200 -> {
                if (view.getSharedView() != null) navigator.navigateToResultFragment(view.getSharedView()!!)
            }
            401 -> {
                view.showInputGroup()
                view.postErrorMessage(stringProvider.getStringResource(R.string.error_unauthorised))
            }
            500 -> {
                view.showInputGroup()
                view.postErrorMessage(stringProvider.getStringResource(R.string.error_server))
            }
        }
    }
}
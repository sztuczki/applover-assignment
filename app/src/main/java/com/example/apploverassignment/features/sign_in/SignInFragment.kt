package com.example.apploverassignment.features.sign_in

import android.os.Bundle
import android.os.Handler
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.transition.Fade
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.getColor
import androidx.fragment.app.Fragment
import com.example.apploverassignment.R
import com.example.apploverassignment.network.RetrofitClient
import com.example.apploverassignment.utils.navigation_utils.Navigator
import com.example.apploverassignment.utils.navigation_utils.NavigatorProvider
import com.example.apploverassignment.utils.string_utils.StringResourceProvider
import com.example.apploverassignment.utils.transition_utils.addOnTransitionEndListener
import kotlinx.android.synthetic.main.fragment_sign_in.*

class SignInFragment : Fragment(), SignInContract.View {

    private lateinit var presenter: SignInContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //TODO: restart/catch request on orientation change
        val retrofitClient = RetrofitClient()
        val stringProvider = StringResourceProvider(activity!!.applicationContext)
        presenter = SignInPresenter(retrofitClient, stringProvider, getNavigator(), this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        //TODO: add/make layout scrollable for horizontal mode
        return inflater.inflate(R.layout.fragment_sign_in, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setOnClickListeners()
        et_password?.transformationMethod = PasswordTransformationMethod.getInstance()
    }

    override fun onResume() {
        super.onResume()
        showInputGroup()
    }

    private fun setOnClickListeners() {
        button_login.setOnClickListener { onLoginButtonClicked() }
        button_password_visibility?.setOnClickListener { onPasswordVisibilityButtonClicked() }
    }

    private fun onLoginButtonClicked() {
        val email = et_email.text?.toString()?.trim()
        val password = et_password.text?.toString()?.trim()
        presenter.postCredentials(email!!, password!!)
    }

    private fun onPasswordVisibilityButtonClicked() {
        changeTransformationMethod()
    }

    private fun changeTransformationMethod() {
        if (et_password?.transformationMethod == PasswordTransformationMethod.getInstance()) {
            et_password?.transformationMethod = HideReturnsTransformationMethod.getInstance()
            button_password_visibility?.setImageResource(R.drawable.ic_preview)
        } else {
            et_password?.transformationMethod = PasswordTransformationMethod.getInstance()
            button_password_visibility?.setImageResource(R.drawable.ic_b_preview)
        }
    }

    override fun hideInputGroup() {
        if (context != null) {
            val transition = Fade(Fade.MODE_OUT).apply { duration = 150 }
            TransitionManager.beginDelayedTransition(sign_in_root, transition)
            text_login?.visibility = View.INVISIBLE
            et_email?.visibility = View.INVISIBLE
            et_password?.visibility = View.INVISIBLE
            button_password_visibility?.visibility = View.INVISIBLE
            button_login?.visibility = View.INVISIBLE
            progress_bar?.visibility = View.VISIBLE
        }
    }

    override fun showInputGroup() {
        //TODO: check if all context checks are necessary, try use Group XML visibility change
        if (context != null) {
            val transition = Fade(Fade.MODE_IN).apply { duration = 150 }
            TransitionManager.beginDelayedTransition(sign_in_root, transition)
            text_login?.visibility = View.VISIBLE
            et_email?.visibility = View.VISIBLE
            et_password?.visibility = View.VISIBLE
            button_password_visibility?.visibility = View.VISIBLE
            button_login?.visibility = View.VISIBLE
            progress_bar?.visibility = View.INVISIBLE
        }
    }

    override fun postErrorMessage(message: String) {
        if (context != null) {
            messageOutAndIn(message, R.color.red_500)
            Handler().postDelayed({
                if (context != null) {
                    val loginMessage = getString(R.string.login)
                    if (text_login.text != getString(R.string.login)) messageOutAndIn(loginMessage, R.color.white)
                }
            }, 2000)
        }
    }

    private fun messageOutAndIn(message: String, colorId: Int) {
        Handler().post {
            if (context != null) {
                val transition = Fade(Fade.OUT).apply { duration = 150 }
                transition.addOnTransitionEndListener { messageIn(message, colorId) }
                TransitionManager.beginDelayedTransition(sign_in_root, transition)
                text_login?.visibility = View.INVISIBLE
            }
        }
    }

    private fun messageIn(message: String, colorId: Int) {
        Handler().post {
            if (context != null) {
                text_login?.setTextColor(getColor(context!!, colorId))
                text_login?.text = message
                val transition = Fade(Fade.MODE_IN).apply { duration = 150 }
                TransitionManager.beginDelayedTransition(sign_in_root, transition)
                text_login?.visibility = View.VISIBLE
            }
        }
    }

    override fun getSharedView(): View? {
        return image_sign_in
    }

    private fun getNavigator(): Navigator {
        return (activity as NavigatorProvider).getNavigator()
    }
}
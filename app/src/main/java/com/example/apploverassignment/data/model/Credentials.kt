package com.example.apploverassignment.data.model

import com.google.gson.annotations.Expose

data class Credentials(@Expose val email: String,
                       @Expose val password: String) {

    override fun toString(): String {
        return "Credentials(email='$email', password='$password')"
    }
}
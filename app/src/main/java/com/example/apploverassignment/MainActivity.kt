package com.example.apploverassignment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.apploverassignment.utils.navigation_utils.FragmentNavigator
import com.example.apploverassignment.utils.navigation_utils.FragmentStore
import com.example.apploverassignment.utils.navigation_utils.Navigator
import com.example.apploverassignment.utils.navigation_utils.NavigatorProvider

class MainActivity : AppCompatActivity(), NavigatorProvider {

    private val fragmentStore = FragmentStore()
    private var navigator: Navigator? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        val fragmentStore = FragmentStore()
//        val navigationProvider = FragmentNavigator(fragmentStore, supportFragmentManager)
        if (savedInstanceState == null) {
            getNavigator().navigateToSignInFragment()
        }
    }

    override fun getNavigator(): Navigator {
        return navigator ?: FragmentNavigator(fragmentStore, supportFragmentManager).also { navigator = it }
    }
}
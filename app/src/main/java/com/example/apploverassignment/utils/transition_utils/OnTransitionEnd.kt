package com.example.apploverassignment.utils.transition_utils

import android.transition.Transition

fun Transition.addOnTransitionEndListener(onTransitionEnd: () -> Unit) {
    this.addListener(object : Transition.TransitionListener {
        override fun onTransitionEnd(transition: Transition?) {
            onTransitionEnd()
        }

        override fun onTransitionResume(transition: Transition?) {}

        override fun onTransitionPause(transition: Transition?) {}

        override fun onTransitionCancel(transition: Transition?) {}

        override fun onTransitionStart(transition: Transition?) {}
    })
}
package com.example.apploverassignment.utils.navigation_utils

import android.view.View

interface Navigator {

    fun navigateToSignInFragment()
    fun navigateToResultFragment(sharedView: View)
}
package com.example.apploverassignment.utils.string_utils

import android.content.Context

class StringResourceProvider(private val context: Context) {

    fun getStringResource(id: Int): String {
        return context.resources.getString(id)
    }
}
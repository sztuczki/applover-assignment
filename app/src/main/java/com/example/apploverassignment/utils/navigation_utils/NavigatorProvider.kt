package com.example.apploverassignment.utils.navigation_utils

interface NavigatorProvider {

    fun getNavigator(): Navigator
}
package com.example.apploverassignment.utils.navigation_utils

import androidx.fragment.app.Fragment
import com.example.apploverassignment.features.result.ResultFragment
import com.example.apploverassignment.features.sign_in.SignInFragment

class FragmentStore {

    fun getSignInFragment(): Fragment {
        return SignInFragment()
    }

    fun getResultFragment(): Fragment {
        return ResultFragment()
    }
}
package com.example.apploverassignment.utils.navigation_utils

import android.view.View
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.apploverassignment.R

class FragmentNavigator(
    private val fragmentStore: FragmentStore,
    private val fragmentManager: FragmentManager
) : Navigator {

    override fun navigateToSignInFragment() {
        val fragment = fragmentStore.getSignInFragment()
        fragmentManager.beginTransaction()
            .replace(R.id.fragment_container, fragment)
            .commit()

    }

    override fun navigateToResultFragment(sharedView: View) {
        val fragment = fragmentStore.getResultFragment()
        fragmentManager.beginTransaction()
            .addSharedElement(sharedView, "testTransitionName")
            .replace(R.id.fragment_container, fragment)
            .addToBackStack(null)
            .commit()
    }
}
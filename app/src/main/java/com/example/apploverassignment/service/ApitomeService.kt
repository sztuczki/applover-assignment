package com.example.apploverassignment.service


import com.example.apploverassignment.data.model.Credentials
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface ApitomeService {

    @POST("/api/v1/login")
    fun signIn(@Body credentials: Credentials): Call<Credentials>
}